//Mini Activity
/*
Setup a basic Express JS server.

*/
//[SECTION] dependencies and modules
	const express = require("express");
	const mongoose = require("mongoose");
	const taskRoute = require("./routes/taskRoute");
	
//[SECTION] server setup
	const app = express();
	const port = 4000; 

//[SECTION] Databse Setup
	mongoose.connect('mongodb+srv://Darrel24:admin123@cluster0.lgbuo.mongodb.net/toDo176?retryWrites=true&w=majority',{

		
		useNewUrlParser:true,
		useUnifiedTopology:true
});

let db = mongoose.connection;
db.on('error',console.error.bind(console,"Connection Error"));
db.once('open',() => console.log(`Connected to MongoDB`));
app.use(express.json());


//Add the task route
app.use("/tasks", taskRoute);


app.listen(port, () => console.log(`Server running at port ${port}`));