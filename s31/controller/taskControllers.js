//Controllers contains the functions and our busines logic of our Express JS Application

const Task = require("../models/task");

//[SECTION] - Controllers

//[SECTION] - Create
module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if (error) {
			return false
		} else {
			return task

		}
	})
}
//[SECTION] - Retrieve
module.exports.getAllTasks = () =>{
	return Task.find({}).then(result =>{
		return result
	})
}
//[SECTION] - Update
	//1. Change status of Task. pending  -> 'completed'
	//we will reference the document using it's ID field.
	module.exports.taskCompleted =(taskId) => {
		//Query/search for the desired task to update.
		//The "findById" mongoose method will look for a resource (task) which matches the ID from the URL of the request
		//upon performing this method inside our collection, a new promise will be instantiated,so we need to be able to handle the possible outcome of that promise.
		return Task.findById(taskId).then((found, error) => {
			//describe how were going to handle the outcome of the promise using a selection control structure.
			//error -> rejected state of the promise
			//process the document found from the collection and change the status from 'pending' => 'completed'
			if (found) {
				//call out the  parameter that describes the result of the query when successful.
				console.log(found); //document found from the db should be displayed in the terminal 
				//modified the status of the retunrned document to 'completed'
				found.status = 'Completed';
				//save the new changes inside our database.
				//upon saving the new changes for this document a 2nd promise will be instantitated
				//we will chain a thenable expression upon performing a save() method into our returned documnet
				return found.save().then((updatedTask, saveErr) => {
					//catch the state of the promise to identify a specific response
					if (updatedTask) {
						return 'Task has been successfully modified';
					} else {
						//return the actual error
						return 'Task failed to Update';
					}
				})
			} else {
				return 'Error! No Document Found ';
			};
		});
	};

	//2. Change the status of the task (Completed -> Pending)
	module.exports.taskPending = (userInput) => {
		//expose this new component
		//search the databse for the user Input.
		//findById() mongoose method to run using the id field as it's reference
		//since performing this method will have 2 possible states/outcome, we will chain a then expression to handle the possible states of the promise
		//assign and invoke this new controller task to its own seperate route.
		return Task.findById(userInput).then((result, err) =>{ //handle and catch of the promise
			if (result) {
				//process the result of the query and extract the property to modify it's value
				result.status = 'Pending';
				//save the new change in the document
				return result.save().then((taskUpdated, error) => {
					//create a controll structure to identify the proper response if the updates was successfully executed
					if (taskUpdated) {
						return `Task ${taskUpdated.name} was updated to Pending`;
					} else {
						return 'Error when saving task updates';
					}
				})
			} else {
				return 'Something Went Wrong';
			}
		});
	}
//[SECTION] - Destroy
	//1. Remove an existing resource inside the TASK collection.
		//expose the data across other modules other app so that it will become reusable.
		// would we need an input from the user? => which resource you want to target
	module.exports.deleteTask = (taskId) => {
		//how will the data will be processed in order to execute the task.
		//select which mongoose method will be used in order to acquirethe desired end goal.
		//Mongoose -> it provides an interface and methods to be able to manipulate the resources found inside the mongoDB atlas
		//in order to identify the location in which the function will be executed append the model name.

		//findByIdAndRemove => this is a mongoose method which targets a document using it's id field and removes the targeted document from the collection.
		//uppon executing this method within our collection a new promise will be instantiated upon waiting for the task to be executed, hwever we need to be able to handle the outcome of the promise whatever state in may fall on.
		//Promises in JS
			//=> Pending (waiting to be executed)
			//=> Fulfilled (successfully executed)
			//=> Rejected (unfullfilled promise)
		//to be able to handle the possible states upon executing the task.	
		//we are going to insert a 'thenable' expression to determin HOW we will respond depending on the result of the promise.
		//identify the 2 possible state of the promise using then expression
		return Task.findByIdAndRemove(taskId).then((fullfilled, rejected) => {
			//identify how you will act according to the outcome.
			if (fullfilled) {
				return 'the Task has been succesfully Removed';
			} else {
				return 'Failed to remove Task';
			};
			//assign a new endpoint for this route.
    	});
	};


