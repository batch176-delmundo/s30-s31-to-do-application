//[SECTION] Dependencies and Modules
	const express = require("express");
	const mongoose = require("mongoose");

//[SECTION] Server Setup
	// establish a connection
	const app = express();
	// define a path/address
	const port = 4000; //Assignt the port to 4000 to avoid conflict in the future with frontend applications that run on port 3000

//[SECTION] Databse Connection
	//connect to mongoDB Atlas
	//change the password bracket to your own database password
	//get the credentials of your atlas user.
	mongoose.connect('mongodb+srv://Darrel24:admin123@cluster0.lgbuo.mongodb.net/toDo176?retryWrites=true&w=majority',{

		//options to add to avoid deprecation warnings because of mongoose/mongodb update.
		useNewUrlParser:true,
		useUnifiedTopology:true


	});

	//Create a notification if the connection to the db is success or a failure:
	let db = mongoose.connection;
	
	//Let's add an on() method from our mongoose connection to show if the connection has succeeded or failed in both the terminal and in the browser for our client.
	db.on('error',console.error.bind(console,"Connection Error"));
	
	//once the connection is open and successful, we will output a message in the terminal:
	db.once('open',() => console.log(`Connected to MongoDB`));

	//Middleware - A middleware, in expressjs context, are methods, functions that acts and adds features to our application.
	app.use(express.json());

	//Schema
	//Before we could create documents from our api to save into our database, we first have to determine the structure of the documents to be written in our database. This is to esnure the consistency of our documents and avoid future errors.

	//Schema acts as a blueprint for our data/document.

	//Schema() constructor from mongoose to create a nre schema object
	const taskSchema = new mongoose.Schema({

		/*

			Define the fields documents.
			We will also be able to determine the appropriate data type of the values.

		*/
		name: String,
		status: String
	});

	//Mongoose Model
	/*
		Models are used to connect your api to the corresponding collection in your database. It is a representation of your collection.

		Models uses schemas to create object that correspond to the shema. By default, when creating the collection from you model, the collection name is pluralized.

		mongoose.model(<nameOfCollectionInAtlas>,<schemaToFollow>)

	*/
	
	const Task = mongoose.model("task",taskSchema);

	
	//Post route to create a new task
	app.post('/tasks',(req, res) =>{

		//When creating a new post/put or any route that requires data from the client, first console log your req.body or any part of the request that contains the data.
		// console.log(req.body);

		//Creating a new task document by using the constructor of our Task model. This construcor should follow the schema of the model.
		let newTask = new Task({

			name: req.body.name,
			status: req.body.status

		});

		//.save() method from an object created by a model.
		//save() method will allow us to save our document by connecting to our collection via our model.

		//1. save() has 2 approaches: we can add an anonymous function to handle the created document or error.
		// 2. we can add .then()chain which will allow us to handle errors and created documents in seperate functions
		/*newTask.save((error,savedTask) =>{

			if (error) {
				res.send("Document creation failed");

			} else {
				res.send(savedTask)
			}

		});*/
		//.then() and .catch() chain
		//.then() is used to handle the proper result/returned value of a function. If the function properly returns a value, we can separate function to handle it.
		//.catch() is used to handle/catch the error from the use of a function. So that if an error occurs, we can handle the error separeately
		newTask.save()
		.then(result => res.send({message: "Document Creation Succesful"}))
		.catch(error => res.send({message: "Error in Document Creation"}));
	});


	// GET method Request to Retrieve all task documents from our collection
	app.get('/tasks', (req, res) =>{

		// TO query using mongoose,first access the model of the collection you want to manipualte
		//Model.find() in mongoose is similar in function to mongoDb's db.collection.find()
		// mongodb - db.tasks.find({})
		Task.find({})
		.then(result => res.send(result))
		.catch(err => res.send(err));
	})


	const sampleSchema = new mongoose.Schema({

		name: String,
		isActive: Boolean


	});

	const Sample = mongoose.model("samples",sampleSchema);
	app.post('/samples', (req, res) =>{

		let newSample = new Sample({
			
			name:req.body.name,
			isActive: req.body.isActive

		});

		newSample.save ((error,savedSample) => {
			if (error) {
				res.send(error);
			} else {
				res.send(savedSample);
			}
		});
	});




/*

Create a new GET request method route to get ALL sample documents
send the array of sample documents in our postman client
Else,catch the erro and send the error in the client

*/

app.get('/samples', (req, res) =>{

		// TO query using mongoose,first access the model of the collection you want to manipualte
		//Model.find() in mongoose is similar in function to mongoDb's db.collection.find()
		// mongodb - db.tasks.find({})
		Sample.find({})
		.then(result => res.send(result))
		.catch(err => res.send(err));
	})



/*
Activity:       

Create a new schema for User. It should have the following fields:
                username,password.
            The data types for both fields is String.

            Create a new model out of your schema and save it in a variable called User

            Create a new POST method route to create a new user document:
                -endpoint: "/users"
                -This route should be able to create a new user document.
                -Then, send the result in the client.
                -Catch an error while saving, send the error in the client.

            Create a new GET method route to retrieve all user documents:
                -endpoint: "/users"
                -Then, send the result in the client.
                -Catch an error, send the error in the client.

*/

const userSchema = new mongoose.Schema({

		username: String,
		password: String
	});

	
	const User = mongoose.model("user",userSchema);

	app.post('/users',(req, res) =>{

		let newUser = new User({

			username: req.body.username,
			password: req.body.password

		});

		
		newUser.save()
		.then(result => res.send({message: "Document Creation Succesful"}))
		.catch(error => res.send({message: "Error in Document Creation"}));
	});



	app.get('/users', (req, res) =>{

		User.find({})
		.then(result => res.send(result))
		.catch(err => res.send(err));
	})






//schema
// const manggagamitSchema = new mongoose.Schema({

// 	username: String,
// 	isAdmin: Boolean
// })

// const Manggagamit = mongoose.model("manggagamit",manggagamitSchema);
// app.post("/mgaManggagamit", (req, res ) => {
// let newManggagamit = new Manggagamit({
// 	username: req.body.username,
// 	isAdmin: req.body.isAdmin
// })
// 	newManggagamit.save((savedManggagamit,error) =>{
// 		if (error){
// 			res.send(error);
// 		} else {
// 			res.send(savedManggagamit);
// 		}
// 	})

// })

	





//[SECTION] Entry Point Response
	//bind the connection to the designated port
	app.listen(port, () => console.log(`Server running at port ${port}`));